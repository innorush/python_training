#!/usr/bin/env python3

"""Evaluate expressions in Polish notation"""

import math

def add(x, y):
    return x+y

def sub(x, y):
    return x-y

def mul(x, y):
    return x*y

def div(x, y):
    return x/y


ops = {"+" : (2, add),
       "-" : (2, sub),
       "*" : (2, mul),
       "/" : (2, div),
       "sin": (1, math.sin)}

def is_op(x):
    return x in ops

def eval_op(op, args):
    f = ops[op][1]
    if len(args) == ops[op][0]:
        return f(*args)
    else:
        raise RuntimeError("insufficient number of arguments in expression")

constants = { 'pi' : 3.14, 'e' : 2.7, 'c' : 3e10 }

def value_of(x):
    if x in constants:
        return constants[x]
    return x

def update_stack(stack, x):
    if is_op(x):
        nargs = ops[x][0]
        args = stack[-nargs:]
        del stack[-nargs:]
        stack.append(eval_op(x, args))
    else:
        stack.append(value_of(x))

def eval_polish1(expr):
    stack = []
    for x in expr:
        update_stack(stack, x)
    return stack


def eval_polish(expr):
    rez = eval_polish1(expr)
    if len(rez) == 1:
        return rez[-1]
    else:
        raise RuntimeError("Incomplete expression")

if __name__=="__main__":
  from sys import argv

  if argv[1]=="--test":
    assert eval_polish1([]) == []
    assert eval_polish1([1]) == [1]
    assert eval_polish1([5]) == [5]
    assert eval_polish1(['pi']) == [3.14]
    assert eval_polish1([5, 7]) == [5, 7]
    assert eval_polish1([5, 7, '+']) == [12]
    assert eval_polish1([5, 7, '-']) == [-2]
    assert eval_polish1([5, 7, '*']) == [35]
    assert eval_polish1([35, 7, '/']) == [5]
    assert eval_polish1([5, 'pi', '+']) == [8.14]
    assert eval_polish1([5, 7, '+', 3, '*', 6]) == [36, 6]

    assert eval_polish([1]) == 1
    assert eval_polish([5]) == 5
    assert eval_polish([5, 7]) == 7
    assert eval_polish([5, 7, '+', 3, '*', 6, 10, '+', '-', 4, '/']) == 5

    assert eval_polish([0, 'sin']) == 0
  else:
    expr=[]
    for x in argv[1:]:
      if is_op(x) or x in constants:
        s=x
      else:
        s=float(x)
      expr.append(s)
    try:
        print(eval_polish(expr))
    except RuntimeError as error:
        print(error)
