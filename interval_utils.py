import math

class Interval:
    def __init__(self, s, e):
        self.s = s
        self.e = e

    def __eq__(self, other):
        return self.s == other.s and self.e == other.e

    def length(self):
        return self.e - self.s

    def overlap(self, i2):
        return not (self.e < i2.s or i2.e < self.s)

    def intersect(self, i2):
        if self.overlap(i2):
            start = max(self.s, i2.s)
            end = min(self.e, i2.e)
            return Interval(start, end)
        else:
            return Interval(math.nan, math.nan)


