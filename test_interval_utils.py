import math
from interval_utils import (
    Interval,
                            )
def test_compar():
    assert Interval(1, 2) == Interval(1, 2)
    assert not (Interval(1, 2) != Interval(1, 2))
    assert Interval(1, 3) != Interval(1, 2)
    assert not(Interval(1, 3) == Interval(1, 2)) 

def test_length():
    assert Interval(-1, 1).length() == 2
    assert  Interval(0, 2).length() == 2
    assert  Interval(2, 2).length() == 0
    
def test_overlap():
    assert Interval(-1, 1).overlap(Interval(0, 2)) == True
    assert Interval(-1, 1).overlap(Interval(2, 3)) == False

def test_intersect():
    assert Interval(-1, 1).intersect(Interval(0, 2)) == Interval(0, 1)
    assert math.isnan(Interval(-1, 1).intersect(Interval(2, 3)).s) 
    assert math.isnan(Interval(-1, 1).intersect(Interval(2, 3)).e) 
